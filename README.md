# kfp-hadrons

We create a data pipeline from Jupyter notebook. 
 
Workflow: 
    
    1) Simulation (dd4hep + Geant4) 
    2) Reconstruction (Marlin)
    3) Hdf5 conversion
    4) Contol plots
    5) Merge all hdf5 files 

we store all data on personal EOS.


1) Open a notebook terminal
2) Authenticate with kerberos
        
```console
kinit <cernid>
```

3) When kerberos has been refreshed, remove any old secret before creating a new one
```console
kubectl delete secret krb-secret
```

4) Create a kerberos secret for Kubernetes
```console
kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000
```

5) Open run_kfp.ipynb in your Notebook server and run the cells
